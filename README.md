Unofficial repository for OpenSurge package on AppImage.

- Improved timer.c
- Physfs, and surgescript libraries optimized with compiler flags
- Little improvement with a flag in Cmakelists
